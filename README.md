# File Upload Flow

## Overview

A lightning flow launched from a button on a details page that allows the user to upload files and associate them with the record or a record related to it.

## Demo (~90 sec)

[Here's a link to a demo video for the flow](https://youtu.be/cPJmySEpi8A)

## Some Screenshots

### Selecting Type of Upload

![Selecting Type of Upload](media/screen1.png)

### Selecting Record and Uploading Files

![Selecting Record and Uploading Files](media/screen2.png)

### Flow

![Flow](media/flow.png)

## Flow Overview

The flow works by following the following steps:

1. Call a custom Apex action to get the Object names for parent and child objects based on the detail page's Id (example: would fetch Account as parent, Case as child on Contact page)
1. Show a screen to select if the user is uploading to the current record, a child, or a parent
1. Show a screen to select the specific record (if child or parent was selected) and upload files to the selected record
1. Refresh data on screen, or navigate to the detail page for the record the user associated the uploaded files with