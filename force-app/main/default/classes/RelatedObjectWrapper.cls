global class RelatedObjectWrapper implements Comparable{
    @AuraEnabled
    global String objectName;
    @AuraEnabled
    global String objectLabel;
    @AuraEnabled
    global Boolean isChild = false;

    public RelatedObjectWrapper(String name, String label) {
        objectName = name;
        objectLabel = label != null ? label : name;
    }

    public RelatedObjectWrapper(String name, String label, Boolean shouldSetChild) {
        objectName = name;
        objectLabel = label != null ? label : name;
        isChild = shouldSetChild;
    }

    global Integer CompareTo(Object ObjToCompare) {
        RelatedObjectWrapper that = (RelatedObjectWrapper)ObjToCompare;
        if (this.objectLabel > that.objectLabel) return 1;
        if (this.objectLabel < that.objectLabel) return -1;
        return 0;
    }
}