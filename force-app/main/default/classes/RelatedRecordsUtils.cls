public with sharing class RelatedRecordsUtils {

    @AuraEnabled(cacheable=true)
    public static List<RecordResult> getExistingRecords(Id recordId, String objectName, Boolean isChild) {
        System.debug('getExistingRecords');
        System.debug('recordId: ' + recordId);
        System.debug('objectName: ' + objectName);
        System.debug('isChild: ' + isChild);
        List<RecordResult> results = new List<RecordResult>();
        List<sObject> queryResult = new List<sObject>();
        if(isChild) {
            String lookupFieldName = 'AccountId'; //ex: AccountId
            String childObjNameField = 'Name'; //ex: Name
            String childObjLabel = 'Contact'; // ex: Contact
            for (ChildRelationship relation : recordId.getSobjectType().getDescribe().getChildRelationships()) {
                if(relation.getChildSObject().getDescribe().getName() == objectName) {
                    if(relation.getRelationshipName() != null) {
                        childObjNameField = getNameFieldByObjectName(objectName);
                        if(childObjNameField != null) {
                            lookupFieldName = relation.getField().getDescribe().getName();
                            childObjLabel = relation.getChildSObject().getDescribe().getLabel();
                        }
                    }
                    break;
                }
            }
            String query = 'SELECT Id, ' + childObjNameField + ' FROM ' + objectName + ' WHERE ' + lookupFieldName + ' = :recordId';
            System.debug(query);
            queryResult = Database.query(query);

            for(sObject eachRecord : queryResult) {
                results.add(new RecordResult((String)eachRecord.get(childObjNameField), (Id)eachRecord.get('Id')));
            }
            for(RecordResult x : results) {
                System.debug(x);
            }
        }
        else {
            String parentFields = objectName + '.Id, ' + objectName + '.' + getNameFieldByObjectName(objectName);
            String originalRecordObjName = recordId.getSobjectType().getDescribe().getName();
            String query = 'SELECT Id, ' + parentFields + ' FROM ' + originalRecordObjName + ' WHERE Id = :recordId';
            System.debug(query);
            queryResult = Database.query(query);

            for(sObject eachRecord : queryResult) {
                String parentName = (String)eachRecord.getSObject(objectName).get(getNameFieldByObjectName(objectName));
                Id parentId = (Id)eachRecord.getSObject(objectName).get('Id');
                results.add(new RecordResult(parentName, parentId));
            }
            for(RecordResult x : results) {
                System.debug(x);
            }
        }


        return results;
    }

    @AuraEnabled(cacheable=true)
    public static String getRecordNameFromId(Id recordId) {
        String objectName = recordId.getSobjectType().getDescribe().getName();
        String nameField = getNameFieldByObjectName(objectName);
        String query = 'SELECT Id, ' + nameField + ' FROM ' + objectName + ' WHERE Id = :recordId';
        sObject obj = Database.query(query);
        return (String)obj.get(nameField);
    }

    private static String getNameFieldByObjectName(String objName) {
        Map<String, String> result = new Map<String, String>();
        FieldDefinition nameField = [SELECT QualifiedApiName, EntityDefinition.QualifiedApiName
                                            FROM FieldDefinition
                                            WHERE EntityDefinition.QualifiedApiName = :objName
                                            AND IsNameField = TRUE LIMIT 1];
        return nameField.QualifiedApiName;
    }





    @AuraEnabled(cacheable=true)
    public static List<ObjectSearchResult> getAvailableObjects()
    {
        System.debug('START getAvailableObjects');
        List<ObjectSearchResult> result = new List<ObjectSearchResult>();
        List<EntityDefinition> entities = [SELECT QualifiedApiName, Label
                                            FROM EntityDefinition
                                            WHERE IsQueryable = True
                                            ORDER BY Label ASC];
        for(EntityDefinition eachEntity : entities) {
            result.add(new ObjectSearchResult(eachEntity));
        }
        System.debug(result);
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<ObjectSearchResult> getAvailableChildObjects(String primaryObjName)
    {
        System.debug('START getAvailableChildObjects');
        List<ObjectSearchResult> result = new List<ObjectSearchResult>();

        SObjectType primaryObjType = ((SObject) Type.forName(primaryObjName).newInstance()).getSObjectType();
        List<String> childObjectNames = new List<String>();
        for (ChildRelationship relation : primaryObjType.getDescribe().getChildRelationships()) {
            if(relation.getRelationshipName() != null) {
                result.add(new ObjectSearchResult(relation));
            }
        }
        System.debug(result);
        return result;
    }

    public class ObjectSearchResult{
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String label {get; set;}
        
        public ObjectSearchResult(EntityDefinition def) {
            name = def.QualifiedApiName;
            label = def.Label;
        }

        public ObjectSearchResult(ChildRelationship childRel) {
            name = childRel.getChildSobject().getDescribe().getName();
            label = childRel.getChildSobject().getDescribe().getLabel();
        }
    }

    public class RecordResult{
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String id {get; set;}

        public RecordResult(String passedLabel, String passedId) {
            label = passedLabel;
            id = passedId;
        }
    }
}
