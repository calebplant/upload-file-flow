global with sharing class GetRelatedObjectsById {

    @InvocableMethod(label='Get Related Objects' description='Returns the list object names and labels corresponding to available child and parent objects for the passed record Id')
    public static List<Result> getRelatedObjects(List<Id> baseId) {
        Result result = new Result();
        String objName = baseId[0].getSObjectType().getDescribe().getName();
        
        Map<String, String> childObjLabelByChildObjName = getAvailableChildObjects(objName);
        Map<String, String> parentObjLabelByParentObjName = getAvailableParentObjects(objName);
        List<RelatedObjectWrapper> relatedObjects = new List<RelatedObjectWrapper>();

        List<RelatedObjectWrapper> childObjects = new List<RelatedObjectWrapper>();
        for(String eachKey : childObjLabelByChildObjName.keySet()) {
            // System.debug(eachLabel);
            childObjects.add(new RelatedObjectWrapper(eachKey, childObjLabelByChildObjName.get(eachKey), true));
        }
        childObjects.sort();
        List<RelatedObjectWrapper> parentObjects = new List<RelatedObjectWrapper>();
        for(String eachKey : parentObjLabelByParentObjName.keySet()) {
            // System.debug(eachLabel);
            parentObjects.add(new RelatedObjectWrapper(eachKey, childObjLabelByChildObjName.get(eachKey)));
        }
        parentObjects.sort();

        result.childObjects = JSON.serialize(childObjects);
        result.parentObjects = JSON.serialize(parentObjects);
        result.objName = baseId[0].getSObjectType().getDescribe().getLabel();
        return new List<Result> {result};
    }

    public static Map<String, String> getAvailableChildObjects(String primaryObjName)
    {
        System.debug('START getAvailableChildObjects');
        Map<String, String> objLabelByObjName = new Map<String, String>();

        SObjectType primaryObjType = ((SObject) Type.forName(primaryObjName).newInstance()).getSObjectType();
        for (ChildRelationship relation : primaryObjType.getDescribe().getChildRelationships()) {
            if(relation.getRelationshipName() != null) {
                objLabelByObjName.put(relation.getChildSObject().getDescribe().getName(), relation.getChildSObject().getDescribe().getLabel());
            }
        }
        // System.debug(objLabelByObjName);
        return objLabelByObjName;
    }

    public static Map<String, String> getAvailableParentObjects(String primaryObjName)
    {
        System.debug('START getAvailableParentObjects');
        Map<String, String> objLabelByObjName = new Map<String, String>();

        SObjectType primaryObjType = ((SObject) Type.forName(primaryObjName).newInstance()).getSObjectType();
        for(Schema.SobjectField strFld: primaryObjType.getDescribe().fields.getMap().Values()) {
            if(strFld.getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                // system.debug('==parent object='+strFld.getDescribe().getReferenceTo());
                objLabelByObjName.put(strFld.getDescribe().getReferenceTo()[0].getDescribe().getName(), strFld.getDescribe().getReferenceTo()[0].getDescribe().getLabel());
            } 
        }

        return objLabelByObjName;
    }


    // private static Map<String, String> getNameFieldsByObjectName(List<String> objNames) {
    //     Map<String, String> result = new Map<String, String>();
    //     List<FieldDefinition> nameFields = [SELECT QualifiedApiName, EntityDefinition.QualifiedApiName
    //                                         FROM FieldDefinition
    //                                         WHERE EntityDefinition.QualifiedApiName IN :objNames
    //                                         AND IsNameField = TRUE];
    //     for(FieldDefinition eachObject : nameFields) {
    //         result.put(eachObject.EntityDefinition.QualifiedApiName, eachObject.QualifiedApiName);
    //     }
    //     return result;
    // }

    global class Result {
        @InvocableVariable(label='Related Child Objects' required=true)
        global String childObjects;
        @InvocableVariable(label='Related Parent Objects' required=true)
        global String parentObjects;
        @InvocableVariable(label='Original Object Type' required=true)
        global String objName;
    }
}
