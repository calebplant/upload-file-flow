import { api, LightningElement } from 'lwc';
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';



export default class FileUpload extends LightningElement {
    
    @api recordId;
    @api recordObjName;
    @api childObjects;
    @api parentObjects;
    @api selectedType;
    @api targetRecordId;
    @api uploadedFileNames;

    selectedRecord;
    uploadedFiles;

    get showChildSelector() {
        return this.selectedType === 'childRecord';
    }

    get showParentSelector() {
        return this.selectedType === 'parentRecord';
    }

    get showReadOnlySelector() {
        return this.selectedType ==='thisRecord';
    }

    get passedChildObjects() {
        return JSON.parse(this.childObjects);
    }

    get passedParentObjects() {
        return JSON.parse(this.parentObjects);
    }

    connectedCallback() {
        console.log('childObjects: ');
        if(this.childObjects) {
            console.log(JSON.parse(this.childObjects));
        }
        console.log('parentObjects: ');
        if(this.parentObjects) {
            console.log(JSON.parse(this.parentObjects));
        }
        console.log('recordId: ' + this.recordId);
        console.log('selectedType: ' + this.selectedType);
    }

    // Handlers
    handleRecordSelect(event) {
        console.log('fileUpload :: handleRecordSelect');
        this.selectedRecord = event.detail;
        try {
            const attributeChangeEvent = new FlowAttributeChangeEvent('targetRecordId', event.detail);
            this.dispatchEvent(attributeChangeEvent);  
    
        } catch(err) {
            console.log(err);
        }
    }
    
    handleUploadFinished(event) {
        console.log('fileUpload :: handleUploadFinished');
        this.uploadedFiles = event.detail.files;
        const attributeChangeEvent = new FlowAttributeChangeEvent('uploadedFileNames', event.detail.files);
        this.dispatchEvent(attributeChangeEvent);
        getRecordNotifyChange([{recordId: this.recordId}]);

        let uploadedFiles = JSON.parse(JSON.stringify(event.detail.files));
        console.log(uploadedFiles);
    }
}