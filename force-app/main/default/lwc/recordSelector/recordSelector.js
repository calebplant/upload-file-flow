import { api, LightningElement, wire } from 'lwc';
import getExistingRecords from '@salesforce/apex/RelatedRecordsUtils.getExistingRecords';
import getRecordNameFromId from'@salesforce/apex/RelatedRecordsUtils.getRecordNameFromId';

export default class RecordSelector extends LightningElement {

    @api recordId;
    @api objectNames;
    @api isChildSelector=false;
    @api recordObjName;

    selectedObjectValue;
    selectedRecordValue;
    recordOptions;
    readOnlyRecordOptions;
    readOnlyRecordSelection;

    get objectOptions() {
        return this.objectNames.map(eachObj => {
            return {label: eachObj.objectLabel, value: eachObj.objectName}
        });
    }

    get showReadOnlyMode() {
        return this.recordName && !this.objectNames;
    }

    get readOnlyNameValue() {
        if(this.showReadOnlyMode) {
            // console.log('read only');
            if(!this.readOnlyRecordSelection) {
                getRecordNameFromId({recordId: this.recordId})
                .then(res => {
                    console.log('Fetched record name: ' + res);
                    this.readOnlyRecordOptions = [{label: res, value: this.recordId}];
                    this.readOnlyRecordSelection = this.recordId;
                    this.dispatchEvent(new CustomEvent('recordchange', {detail: this.recordId}));
                })
                .catch(err => {
                    console.log('Error fetching name from Id!');
                    console.log(err);
                })
            }
        }
        return this.readOnlyRecordSelection;
    }

    get readOnlyObjectOptions() {
        return [{label: this.recordObjName, value: this.recordObjName}];
    }

    get readOnlyObjectValue() {
        return this.recordObjName;
    }

    @wire(getRecordNameFromId, {recordId: '$recordId'})
    recordName;

    // Handlers
    handleSelectionChange(event) {
        console.log('RecordSelector :: handleSelectionChange');
        console.log(event.detail.value);
        console.log(event.currentTarget.name);
        switch(event.currentTarget.name) {
            case 'objectName':
                this.selectedObjectValue = event.detail.value;
                let payload = {
                    recordId: this.recordId,
                    objectName: event.detail.value,
                    isChild: this.isChildSelector,
                }
                console.log('payload: ');
                console.log(payload);
                getExistingRecords(payload)
                .then(res => {
                    console.log('response from server:');
                    console.log(res);
                    this.recordOptions = res.map(eachRecord => {
                        return {label: eachRecord.label, value: eachRecord.id}
                    });
                })
                .catch(err => {
                    console.log('error fetching records:');
                    console.log(err);
                })
                break;
            case 'recordId':
                this.selectedRecordValue = event.detail.value;
                this.dispatchEvent(new CustomEvent('recordchange', {detail: event.detail.value}));
                break;
        }
    }
}